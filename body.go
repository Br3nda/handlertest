package handlertest

import (
	"bytes"
	"fmt"
	"io"
	"mime/multipart"
	"net/url"
	"strings"
	"testing"
)

func (r *Request) getBodyReader(t *testing.T) io.Reader {
	if r.files != nil {
		// multipart encoding

		var b bytes.Buffer
		var fw io.Writer
		var err error

		w := multipart.NewWriter(&b)
		for field, values := range r.fields {
			for _, v := range values {
				if fw, err = w.CreateFormField(field); err != nil {
					t.Error(err)
				}
				if _, err = fmt.Fprint(fw, v); err != nil {
					t.Error(err)
				}
			}
		}

		for field, files := range r.files {
			for filename, reader := range files {
				if c, isCloser := reader.(io.Closer); isCloser {
					defer func() {
						handle(t, c.Close())
					}()
				}
				if fw, err = w.CreateFormFile(field, filename); err != nil {
					t.Error(err)
				}
				if _, err = io.Copy(fw, reader); err != nil {
					t.Error(err)
				}
			}
		}
		handle(t, w.Close())
		r.ContentType(w.FormDataContentType())

		return &b
	}

	// if json or url-encoded body, just return it
	return strings.NewReader(r.body)
}

// JSON Sets given json as body and adds `Content-Type: application/json` header
func (r *Request) JSON(json string) *Request {
	r.body = json
	return r
}

// FormURLEncoded Encodes form values in request's body.
// Content-Type will be `application/x-www-form-urlencoded`
func (r *Request) FormURLEncoded(values url.Values) *Request {
	r.body = values.Encode()

	if r.method == "" {
		r.method = "POST"
	}
	return r.ContentType(ContentTypeFormURLEncoded)
}

// FormURLEncodedMap Encodes form values in request's body.
// Using simple map in case your fields don't have multiple values.
// Content-Type will be `application/x-www-form-urlencoded`
func (r *Request) FormURLEncodedMap(values map[string]string) *Request {
	return r.FormURLEncoded(ValuesFromMap(values))
}

// FileReaders Sets several files with contents from io.Reader
// Content-Type will be `multipart/form-data`
func (r *Request) FileReaders(fields map[string]map[string]io.Reader) *Request {
	r.files = fields
	return r
}

// Files Sets several files with text contents
// Content-Type will be `multipart/form-data`
func (r *Request) Files(fields map[string]map[string]string) *Request {
	flds := make(map[string]map[string]io.Reader)
	for fld, files := range fields {
		readersMap := make(map[string]io.Reader)
		for name, content := range files {
			readersMap[name] = strings.NewReader(content)
		}
		flds[fld] = readersMap
	}

	r.files = flds
	return r
}

// File Sets a file on a given field with text contents
// Content-Type will be `multipart/form-data`
func (r *Request) File(field string, fileName string, content string) *Request {
	return r.FileReader(field, fileName, strings.NewReader(content))
}

// FileReader Sets a file on a given field with contents from io.Reader
// Content-Type will be `multipart/form-data`
func (r *Request) FileReader(field string, fileName string, content io.Reader) *Request {
	if r.files == nil {
		r.files = make(map[string]map[string]io.Reader)
	}
	if _, exists := r.files[field]; !exists {
		r.files[field] = make(map[string]io.Reader)
	}
	r.files[field][fileName] = content

	return r
}

// FormMultipart Encodes form values request's body
// Content-Type will be `multipart/form-data`
func (r *Request) FormMultipart(fields url.Values) *Request {
	if r.files == nil {
		// presence of r.files says it will be multipart form
		r.files = make(map[string]map[string]io.Reader)
	}
	r.fields = fields
	return r
}

// FormMultipartMap Encodes form values request's body
// Content-Type will be `multipart/form-data`
func (r *Request) FormMultipartMap(values map[string]string) *Request {
	return r.FormMultipart(ValuesFromMap(values))
}
