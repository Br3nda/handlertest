package handlertest

import (
	"net/url"
	"testing"
)

func handle(t *testing.T, err error) {
	if err != nil {
		t.Error(err)
	}
}

// ValuesFromMap converts map string to url.Values
func ValuesFromMap(values map[string]string) url.Values {
	v := url.Values{}
	for key, val := range values {
		v.Set(key, val)
	}
	return v
}
