// Package handlertest is a toolkit for testing http handlers in a verbose way.
//
// See assert subpackage handling assertions on http.Response
// https://godoc.org/gitlab.com/gotests/handlertest/assert
package handlertest
