# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## [1.1.0] - 2020-03-30
### Added
- Renamed (with backward compat) `Custom` methods to more verbose `Request` and `Response` #18
- Added README section explaining integration with Gorilla MUX #8
- Added example for testing multiple statuses and bodies #10

## [1.0.0] - 2019-09-30
### Added
- Most needed (in my opinion) assertions and request creation helpers. Complex ones include:
  - Send a form with files
  - Assert parsed contents of returned JSON
  - Support diffing JSON
- Full documentation (in godoc, and quickref in README)
- Full tests
- Docker + Gitlab CI builds
- Quite big set of passing linters
- No dependencies + interfaces to plug them in
