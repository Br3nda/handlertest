package handlertest

import "net/http"

// Request Use it implement custom modifications of request not covered by this lib use the below function.
// Please also file an issue to support your case if it is general enough.
func (r *Request) Request(customize func(request *http.Request) *http.Request) *Request {
	r.custom = customize
	return r
}

// Custom deprecated in favor of Response
func (r *Request) Custom(customize func(request *http.Request)) *Request {
	return r.Request(func(request *http.Request) *http.Request {
		customize(request)
		return request
	})
}
