# Run with DOCKER_BUILDKIT=1 docker build .

FROM golang:1.13-alpine AS base
MAINTAINER Krzysztof Madejski <krzysztof@madejscy.pl>

# Install tooling
RUN apk --no-cache add build-base
RUN set -o pipefail && wget -O - -q https://raw.githubusercontent.com/golangci/golangci-lint/master/install.sh | sh -s v1.19.1 -d /usr/bin

WORKDIR /build

# Copy go mod and sum files
COPY go.mod go.sum ./
# Download all dependencies. Dependencies will be cached if the go.mod and go.sum files are not changed
RUN go mod download

# Get code
COPY . .

FROM base AS dev
# Run linters
# TODO this should be run script, not another image layer
# TODO align it with what's in gitlab-CI
RUN golangci-lint run --enable-all -D whitespace -D gochecknoglobals -D godox

# Run tests
RUN go test -v ./... -cover
