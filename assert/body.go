package assert

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"os"
	"reflect"
	"strconv"
	"testing"
)

// CompactJSONb returns JSON compacted to one line
func CompactJSONb(jsonBytes []byte, t *testing.T) string {
	dst := new(bytes.Buffer)
	if err := json.Compact(dst, jsonBytes); err != nil {
		t.Error(err)
		return ""
	}

	return dst.String()
}

// CompactJSON returns JSON compacted to one line
func CompactJSON(jsonStr string, t *testing.T) string {
	return CompactJSONb([]byte(jsonStr), t)
}

// IndentJSONb pretty-prints JSON
func IndentJSONb(jsonBytes []byte, t *testing.T) string {
	dst := new(bytes.Buffer)
	if err := json.Indent(dst, jsonBytes, "", "\t"); err != nil {
		t.Error(err)
		return ""
	}

	return dst.String()
}

// IndentJSON pretty-prints JSON
func IndentJSON(jsonStr string, t *testing.T) string {
	return IndentJSONb([]byte(jsonStr), t)
}

func shouldDiff() bool {
	ret, err := strconv.ParseBool(os.Getenv("HANDLERTEST_DIFF"))
	if err != nil {
		return false // default
	}
	return ret
}

// Body Assert that handler returned a specific body.
func (a *Assert) Body(test func(t *testing.T, responseBody []byte)) *Assert {
	body, err := ioutil.ReadAll(a.R.Body)
	if err != nil {
		a.T.Errorf("Could not read response body: %v", err)
	}

	test(a.T, body)

	return a
}

// JSON Assert that the response equals to given JSON.
// Indentation here doesn't play a role.
//
// To show a diff between expected and actual values set `HANDLERTEST_DIFF=true` env variable
func (a *Assert) JSON(expectedContent string) *Assert {
	// TODO contenttype.JSON
	return a.ContentType("application/json").Body(func(t *testing.T, body []byte) {
		if expectedContent == "" {
			t.Errorf("Empty string is not a valid json")
			return
		}

		var actual string
		if shouldDiff() {
			if differ != nil {
				expectedContent = IndentJSON(expectedContent, t)
				actual = IndentJSONb(body, t)

				if expectedContent != "" && actual != "" && expectedContent != actual {
					diff := differ(expectedContent, actual)
					t.Error("Expected JSON response -expected,+actual ", diff)
				}
				return
			}
			t.Errorf("You set HANDLERTEST_DIFF, but didn't configure Differ with handlertest.SetDiff(d Differ)")
		}

		// compact version
		expectedContent = CompactJSON(expectedContent, t)
		actual = CompactJSONb(body, t)

		if expectedContent != "" && actual != "" && expectedContent != actual {
			t.Errorf("Expected JSON response '%s', but got '%s", expectedContent, actual)
		}
	})
}

// JSONUnmarshallsTo Assert that JSON response unmarshalls to given object / list
func (a *Assert) JSONUnmarshallsTo(obj interface{}) *Assert {
	return a.ContentType("application/json").Body(func(t *testing.T, body []byte) {

		objPtr := reflect.New(reflect.TypeOf(obj))
		if err := json.Unmarshal(body, objPtr.Interface()); err != nil {
			t.Errorf("Could not unmarshall json body to %T", obj)
		}
	})
}

// JSONMatches Asserts that JSON response unmarshalls to object given as 2nd argument in the function
// further asserting conditions on the returned objects.
//
// In case it's hard to predict your whole response,
// or you don't want to test the whole response, you might use this function
// to get an unmarshalled response body and test for specific values.
func (a *Assert) JSONMatches(test interface{}) *Assert {
	return a.Body(func(t *testing.T, body []byte) {
		var nilT *testing.T

		f := reflect.ValueOf(test)
		if f.Kind() != reflect.Func || f.Type().NumIn() != 2 || f.Type().In(0) != reflect.TypeOf(nilT) {
			t.Errorf("Function passed to JsonConformsTo should have func(*testing.T, expectedType) ..")
			return
		}

		objPtr := reflect.New(f.Type().In(1))
		if err := json.Unmarshal(body, objPtr.Interface()); err != nil {
			t.Errorf("Could not unmarshall json body to %T", objPtr.Elem().Interface())
		}

		f.Call([]reflect.Value{
			reflect.ValueOf(t),
			objPtr.Elem(),
		})
	})
}
