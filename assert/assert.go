package assert

import (
	"net/http"
	"testing"
)

// Assert Specifies how the http.Response should look like by chaining assertions on it.
// Assertions are evaluated immediately.
type Assert struct {
	R *http.Response
	T *testing.T
}

func (a *Assert) TestRun() func(*testing.T) {
	return func(t *testing.T) {
		// TODO test it
	}
}

// Response Use it to implement custom assertions you might need that is not covered by this lib use below function.
// Please also file an issue to support your case if it is general enough.
func (a *Assert) Response(customTest func(t *testing.T, response *http.Response)) *Assert {
	customTest(a.T, a.R)

	return a
}

// Custom deprecated in favor of Response
func (a *Assert) Custom(customTest func(t *testing.T, response *http.Response)) *Assert {
	return a.Response(customTest)
}
