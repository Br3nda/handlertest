package assert_test

import (
	"fmt"
	"net/http"
	"strconv"
	"testing"

	"gitlab.com/gotests/handlertest"
)

//nolint:funlen
func ExampleAssert_Status_multiple() {
	AHandler := func(w http.ResponseWriter, r *http.Request) {
		statusStr := r.Header.Get("X-Return")
		status, err := strconv.Atoi(statusStr)
		var response string

		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			response = `{"code": "WRONG_INPUT"}`
		} else {
			w.WriteHeader(status)
			response = `{"id": 1}`
		}

		_, err = w.Write([]byte(response))
		if err != nil {
			fmt.Print(err)
		}
	}

	type successfulResponse struct {
		ID int `json:"id"`
	}
	type errorResponse struct {
		Code string `json:"code"`
	}

	t := new(testing.T)

	type expected struct {
		status      int
		jsonMatches interface{}
	}

	tests := []struct {
		name     string
		status   string
		expected expected
	}{{
		name:   "Status 200",
		status: "200",
		expected: expected{
			status: 200,
			jsonMatches: func(t *testing.T, ret successfulResponse) {
				if ret.ID != 1 {
					t.Error("Expected id=1")
				}
			},
		},
	}, {
		name:   "Error",
		status: "awfa",
		expected: expected{
			status: 400,
			jsonMatches: func(t *testing.T, ret errorResponse) {
				if ret.Code != "WRONG_INPUT_DATA" {
					t.Errorf("Expected WRONG_INPUT_DATA, got %s", ret.Code)
					fmt.Printf("Expected WRONG_INPUT_DATA, got %s", ret.Code)
				}
			},
		},
	},
	}

	for _, tt := range tests {
		// for somereason example don't like t.Run
		//t.Run(tt.name, func(t *testing.T) {
		handlertest.Call(AHandler).Header("X-Return", tt.status).
			Assert(t).
			Status(tt.expected.status).
			JSONMatches(tt.expected.jsonMatches)
		//})
	}

	// Output: Expected WRONG_INPUT_DATA, got WRONG_INPUT
}
