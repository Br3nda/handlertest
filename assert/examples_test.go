package assert_test

import (
	"fmt"
	"net/http"
	"testing"

	"gitlab.com/gotests/handlertest"
)

func ExampleAssert_JSONMatches() {
	ProductListControllerThatLikesCategoryB := func(w http.ResponseWriter, r *http.Request) {
		_, _ = w.Write([]byte(`[{"category":"b"}]`))
	}

	type Product struct {
		Category string
	}

	// TODO mock t in #12 to test for returned value
	t := new(testing.T)

	// create your request
	handlertest.Call(ProductListControllerThatLikesCategoryB).GET("/products?category=a").
		// then assert your expectations
		Assert(t).
		Status(http.StatusOK).
		JSONMatches(func(t *testing.T, products []Product) {
			// unmarshalling of JSON objects is done for you
			if len(products) == 0 {
				t.Errorf("Expected to have some products returned")
			}
			for _, p := range products {
				if p.Category != "a" {
					t.Errorf("Expected filter to return products only of category %s, but got %s",
						"a", p.Category)

					fmt.Printf("Expected filter to return products only of category %s, but got %s",
						"a", p.Category)
				}
			}
		})

	// Output: Expected filter to return products only of category a, but got b
}
