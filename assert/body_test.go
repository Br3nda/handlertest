package assert_test

import (
	"encoding/json"
	"net/http"
	"os"
	"testing"

	"gitlab.com/gotests/handlertest"
	"gitlab.com/gotests/handlertest/assert"
)

func setBody(t *testing.T, content string) func(w http.ResponseWriter, r *http.Request) {
	handler := func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", handlertest.ContentTypeJSON)

		if _, err := w.Write([]byte(content)); err != nil {
			t.Error(err)
		}
	}
	return handler
}

func TestExpectsBodyFunction(t *testing.T) {
	mockT := new(testing.T)
	handlertest.Call(setBody(t, `[{"id": 201809}]`)).Assert(mockT).
		Body(func(t *testing.T, body []byte) {
			// don'T raise error on mockT
		})
	if mockT.Failed() {
		t.Errorf("Expected assertion to pass")
	}
}

type Obj struct {
	ID int `json:"id"`
}

func TestExpectsBodyFunctionFails(t *testing.T) {
	mockT := new(testing.T)
	handlertest.Call(setBody(t, `[{"id": 201809}]`)).Assert(mockT).
		Body(func(t *testing.T, body []byte) {
			var o Obj
			if err := json.Unmarshal(body, &o); err != nil {
				t.Errorf("Could not unmarshall body")
				return
			}
			if o.ID > 201807 {
				mockT.Errorf("Expected Id to be something it wasn'T")
			}
		})
	if !mockT.Failed() {
		t.Errorf("Expected assertion to fail")
	}
}

// TODO charset: utf-8 in Content-Type
func TestExpectsJSON(t *testing.T) {
	mockT := new(testing.T)
	handlertest.Call(setBody(t, `[]`)).Assert(mockT).
		JSON(`[]`)
	if mockT.Failed() {
		t.Errorf("Expected assertion to pass")
	}
}

func TestExpectsJsonBodyFails(t *testing.T) {
	mockT := new(testing.T)
	handlertest.Call(setBody(t, `[]`)).Assert(mockT).
		JSON(`[{"id": 1}]`)
	if !mockT.Failed() {
		t.Errorf("Assertion should fail when body is different")
	}
}

func TestExpectsJsonBodyIndentHoweverYouLike(t *testing.T) {
	mockT := new(testing.T)
	handlertest.Call(setBody(t, `[{"id": 
1, "someOtherField": "and its content"}]`)).Assert(mockT).
		JSON(`[
  {
    "id": 1,
    "someOtherField": "and its content"
  }
]`)
	if mockT.Failed() {
		t.Errorf("Expected assertion to pass")
	}
}

func TestExpectsJsonBodyFailsIfNotValidJSON(t *testing.T) {
	mockT := new(testing.T)
	handlertest.Call(setBody(t, `[{]`)).Assert(mockT).
		JSON(`[]`)
	if !mockT.Failed() {
		t.Errorf("Expected assertion to fail")
	}
}

func TestExpectsJsonDiffCalled(t *testing.T) {
	mockT := new(testing.T)
	if err := os.Setenv("HANDLERTEST_DIFF", "true"); err != nil {
		t.Fatal(err)
	}

	a := handlertest.Call(setBody(t, `[{"id": 
1, "someOtherField": "and it content"}]`)).Assert(mockT)

	var differCalled bool
	assert.SetDiff(func(minusPrefixed, plusPrefixed interface{}) string {
		differCalled = true
		return "+"
	})

	a.JSON(`[
  {
    "id": 1,
    "someOtherField": "and its content"
  }
]`)
	if !mockT.Failed() {
		t.Errorf("Expected assertion to fail")
	}
	if !differCalled {
		t.Errorf("Expected Differ to be called")
	}

	assert.SetDiff(nil)
	if err := os.Setenv("HANDLERTEST_DIFF", "false"); err != nil {
		t.Fatal(err)
	}
}

func TestExpectsJsonDiffNotSet(t *testing.T) {
	mockT := new(testing.T)
	if err := os.Setenv("HANDLERTEST_DIFF", "true"); err != nil {
		t.Fatal(err)
	}

	a := handlertest.Call(setBody(t, `[]`)).Assert(mockT)
	a.JSON(`[]`)

	if !mockT.Failed() {
		t.Errorf("Expected assertion to fail because differ is not set")
	}

	if err := os.Setenv("HANDLERTEST_DIFF", "false"); err != nil {
		t.Fatal(err)
	}
}

func TestExpectJsonType(t *testing.T) {
	mockT := new(testing.T)
	handlertest.Call(setBody(t, `[{"id": 1}]`)).Assert(mockT).
		JSONUnmarshallsTo([]Obj{})
	if mockT.Failed() {
		t.Errorf("Expected assertion to pass")
	}
}

func TestExpectJsonTypeFails(t *testing.T) {
	mockT := new(testing.T)
	handlertest.Call(setBody(t, `{"id": 1}`)).Assert(mockT).
		JSONUnmarshallsTo([]Obj{})
	if !mockT.Failed() {
		t.Errorf("Expected assertion to fail")
	}
}

func TestExpectJSONMatches(t *testing.T) {
	mockT := new(testing.T)
	handlertest.Call(setBody(t, `[{"id": 1}]`)).Assert(mockT).
		JSONMatches(func(t *testing.T, list []Obj) {
			if len(list) != 1 {
				t.Errorf("Expected length 0")
			}
			if len(list) < 1 || list[0].ID != 1 {
				t.Errorf("Expected list[0].id=1")
			}
		})

	if mockT.Failed() {
		t.Errorf("Expected assertion to pass")
	}
}

func TestExpectJsonMatchesCantUnmarshall(t *testing.T) {
	mockT := new(testing.T)
	handlertest.Call(setBody(t, `[{"id": 1}]`)).Assert(mockT).
		JSONMatches(func(t *testing.T, obj Obj) {})
	if !mockT.Failed() {
		t.Errorf("Expected assertion to fail")
	}
}

func TestExpectJsonMatchesFails(t *testing.T) {
	mockT := new(testing.T)
	handlertest.Call(setBody(t, `[{"id": 1}]`)).Assert(mockT).
		// TODO allow to use pointers also JSONMatches(func(T *testing.T, list *[]Obj) {
		JSONMatches(func(t *testing.T, list []Obj) {
			t.Errorf("Fail because something didn'T meet your expectations")
		})
	if !mockT.Failed() {
		t.Errorf("Expected assertion to fail")
	}
}

func TestExpectJsonMatchesWrongFunc(t *testing.T) {
	type test struct {
		name     string
		function interface{}
	}
	for _, tt := range []test{
		{"Empty func", func() {}},

		{"T should be the first arg", func(Obj, Obj) {}},
		{"T should be a pointer", func(testing.T, Obj) {}}, //nolint:govet // let vet complain about copied mutex
		{"Too many params", func(*testing.T, Obj, Obj) {}},
	} {
		t.Run(tt.name, func(t *testing.T) {
			mockT := new(testing.T)
			handlertest.Call(setBody(t, `[{"id": 1}]`)).Assert(mockT).
				JSONMatches(tt.function) //nolint:scopelint // function is evaluated straight away in this loop
			if !mockT.Failed() {
				t.Errorf("Expected assertion to fail")
			}
		})
	}
}

// TODO ConformsToFails on unexported declaration

// TODO test json requests if ContentType not set properly
