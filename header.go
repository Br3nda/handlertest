package handlertest

// Header Sets a header
func (r *Request) Header(key string, value string) *Request {
	r.headers.Set(key, value)
	return r
}

// ContentType Shorthand for setting `Content-Type`
func (r *Request) ContentType(contentType string) *Request {
	return r.Header("Content-Type", contentType)
}

// ContentTypeFormURLEncoded application/x-www-form-urlencoded
const ContentTypeFormURLEncoded = "application/x-www-form-urlencoded"

// ContentTypeMultipartFormDataPrefix multipart/form-data;
const ContentTypeMultipartFormDataPrefix = "multipart/form-data;"

// ContentTypeJSON application/json
const ContentTypeJSON = "application/json"
