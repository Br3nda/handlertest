[![Build Status](https://gitlab.com/gotests/handlertest/badges/master/build.svg)](https://gitlab.com/gotests/handlertest/commits/master)
[![Coverage Report](https://gitlab.com/gotests/handlertest/badges/master/coverage.svg)](https://gitlab.com/gotests/handlertest/commits/master)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/gotests/handlertest)](https://goreportcard.com/report/gitlab.com/gotests/handlertest)

[![GoDoc](https://godoc.org/gitlab.com/gotests/handlertest?status.svg)](https://godoc.org/gitlab.com/gotests/handlertest)
[![License MIT](https://img.shields.io/badge/License-MIT-brightgreen.svg)](https://choosealicense.com/licenses/mit/)


# Handlertest

Toolkit for testing http handlers in a verbose way.

Two basic examples:
```go
func TestListFilter(t *testing.T) {
	// do some inserts to test DB

	type Product struct {
		Category string
	}

	// create your request
	handlertest.Call(YourHandler).GET("/products?category=a").
		// then assert your expectations
		Assert(t).
		Status(http.StatusOK).
		JSONMatches(func(t *testing.T, products []Product) {
			// unmarshalling of JSON objects is done for you
			if len(products) == 0 {
				t.Errorf("Expected to have some products returned")
			}
			for _, p := range products {
				if p.Category != "a" {
					t.Errorf("Expected filter to return products only of category %s, but got %s", 
						"a", p.Category)
				}
			}
		})
}

// or

func TestUploadAttachments(t *testing.T) {
	// create request
	handlertest.Call(blog.uploadAttachments).
		POST("/attachments").
		FormMultipartMap(map[string]string{
			"post_id": "1",
		}).
		File("files[]", "img1.jpg", "contents").
		// then assert your expectations
		Assert(t).
		Status(http.StatusCreated).
		ContentType("text/html")
}
```

## Semver

This library follows [semantic versioning](https://semver.org/), [semver in go](https://github.com/golang/go/wiki/Modules#semantic-import-versioning).

You might want to check the list of [available versions](https://gitlab.com/gotests/handlertest/-/tags) and [CHANGELOG](CHANGELOG.md).

## Quick reference

### Request

Test request is created with `handlertest.Call(YourHttpHandler)`. 

Then you can set how the request should look like by chaining methods. 
```
handlertest.Call(YourHttpHandler).POST("/jobs").JSON(`{"name": "test"}`)
```

Methods below are part of `Request`.

#### Methods

- `Method("PUT")` - Call handler with a given method.
- `POST()` - shorthand for a POST method
- `GET()` - shorthand for a GET method

#### Url

- `URL("/jobs?status=SUCCESSFUL")` - set request's URL

#### Body

- ```JSON(`{"name": "test"}`)``` - sets given json as body and adds `Content-Type: application/json` header.

#### Forms

A set of methods that encodes form values in a body. 
Methods creating forms of `Content-Type: application/x-www-form-urlencoded` 
- `FormURLEncoded(values url.Values)` - general method to set form fields
- `FormURLEncodedMap(values map[string]string)` - a shorthand accepting single strings as field values

Methods creating forms of `Content-Type: multipart/form-data`:
- `FormMultipart(fields url.Values)` - general method to set form fields that are not files
- `FormMultipartMap(values map[string]string)` - shorthand for the above
- `File(field string, fileName string, content string)` - adds a file to a given field
- `FileReader(field string, fileName string, content io.Reader)` - adds a file to a given field taking content from a reader
- `Files(fields map[string]map[string]string)` sets all files at once
- `FileReaders(fields map[string]map[string]io.Reader)` - sets all files at once

#### Headers

- `Header(key string, value string)` - sets a header
- `ContentType(contentType string)` - shorthand for setting `Content-Type`

#### Context

*TBDone*

#### Custom modifications

Call `Request` to set a custom Request with modifications not covered by this lib use the below function. Please also file an issue to support your case if it is general enough.
- `Request(func(request *http.Request) *http.Request)`

#### Integration with Gorilla Mux

Tested code at https://gitlab.com/gotests/handlertest-gorilla-mux:

```go
func Example_gorilla_mux_test_routes() {
	PrintMuxVar := func(w http.ResponseWriter, r *http.Request) {
		vars := mux.Vars(r)
		fmt.Printf("%v", vars)
	}

	t := new(testing.T)

	router := mux.NewRouter()
	router.HandleFunc("/products/{key}", PrintMuxVar)

	handlertest.Call(router.ServeHTTP).GET("/products/library").
		Assert(t)

	// Output: map[key:library]
}

func Example_gorilla_mux_inject_vars() {
	PrintMuxVar := func(w http.ResponseWriter, r *http.Request) {
		vars := mux.Vars(r)
		fmt.Printf("%v", vars)
	}

	t := new(testing.T)

	handlertest.Call(PrintMuxVar).Request(func(r *http.Request) *http.Request {
		return mux.SetURLVars(r, map[string]string{
			"key": "value",
		})
	}).Assert(t)

	// Output: map[key:value]
}

```


## Assert 

Once you created a needed request call on it `.Assert(t)` to get get an object where you can specify assertions.

```go
func TestPostForm(t *testing T) {
  handlertest.Call(yourHandler).FormURLEncodedMap(map[string]string{
    "field": "value"
  }).Assert(t).
    Status(http.StatusCreated).   .
    ContentType("text/html")
} 
```

#### Status

- `Status(statusCode int)` - assert that response has specific HTTP Status Code

#### Headers

- `Header(key string, value string)` - assert that specific header is set
- `HeaderMissing(key string)` - assert that specific header is not set
- `ContentType(contentType string)` - assert that response is of specific `Content-Type`

#### Body

There is one general function that lets you assert that handler provided a specific body:
- `Body(func(t *testing.T, body []byte))`
		
To support asserting for json response common in API development there are following assertions that tests that Content-Type is set right and offer different ways to assert for the body contents: 
- ```JSON(`[{"id": 1}]`)``` - providing it as a string. Indentation here doesn't play a role and there will be an option to show diff between expected and actual values.
- `JSONUnmarshallsTo([]Obj{})` - there is simple assertion that tests unmarshalling 
- `JSONMatches(func(t *testing.T, ret []Obj)` - in case it's hard to predict your whole response, 
or you don't want to test the whole response, you might use this function 
to get an unmarshalled response body and test for specific values.

#### Context

#### Custom modifications

Use it to implement custom assertions you might need that are not covered by this lib use below function. Please also file an issue to support your case if it is general enough
```go
a.Response(func(t *testing.T, response *http.Response) {
    t.Error("I don't like this response")
})
```

## Contributing

Your issues and PRs won't be disregarded. Please do keep them coming.

There is hopefully a low-level entry barrier to become maintainer and owner of the project. There are three levels to go through:  
- [Reporters](https://gitlab.com/gotests/reporters) - Anyone who report a bug or suggest an improvement is welcomed
- [Reviewes](https://gitlab.com/gotests/reviewers) - Shaping development by reviewing PRs
- [Owners](https://gitlab.com/gotests/owners) - Deciding on lib development. Accepting PRs

Kudos go to:
- [@rafal-brize](https://github.com/rafal-brize) for hardware support 
- [@joncfoo](https://github.com/joncfoo) for improvement suggestions
- members of the Warsaw Golang Meetup for warm welcome and initial reviews 